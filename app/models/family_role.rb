class FamilyRole < ApplicationRecord
  serialize :values, Array
  belongs_to :family
  belongs_to :role

  validates :family, uniqueness: { scope: :role_id }

  def self.get_skills(family_name, role_name)
    FamilyRole.find_by(family: Family.find_by(name: family_name), role: Role.find_by(name: role_name))&.values
  end
end
