class CreateFamilyRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :family_roles do |t|
      t.text :values
      t.references :family, foreign_key: true
      t.references :role, foreign_key: true

      t.timestamps
    end
  end
end
