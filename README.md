# README

### Import data
`rake populate:skills_data` - To populate data from CSV

### Get the list of skills for family and role combination
`FamilyRole.get_skills('Marketing', 'VP')`

`output ->  [{:name=>"LXP", :value=>"I"}, {:name=>"MyGuide", :value=>"I"}, {:name=>"Marketplace", :value=>"I"}, {:name=>"Project Mgmt, Execution Skills/W3 Orientation", :value=>"A"}, {:name=>"Data Fluency & Analytics Skills, Metrics Orientation", :value=>"A"}, {:name=>"Leadership:  Strategic Thinking, Goal Setting, Inspiring, Execute with accountability, Timely Updates ", :value=>"A"}, {:name=>"Collaboration/Team-Player/ Inter-personal skills", :value=>"A"}, {:name=>"Presentation & Communication Skills", :value=>"A"}, {:name=>"Digital Marketing & Social Media", :value=>"A"}, {:name=>"Customer Interactions & Reltationships", :value=>"A"}, {:name=>"Domain Skills/ Tools", :value=>"A"}, {:name=>"Tech / Engineering Skills", :value=>"I"}, {:name=>"Attention to Details, Going the Extra Mile", :value=>"A"}, {:name=>"Sense of Urgency/ Efficiency", :value=>"A"}, {:name=>"EQ (Self-awareness, etc)", :value=>"A"}, {:name=>"Self-initiaitve, Owner-mindset/ Problem-Solving", :value=>"A"}, {:name=>"Learnability/ Lifelong Learning Score on Spark", :value=>"A"}]`
