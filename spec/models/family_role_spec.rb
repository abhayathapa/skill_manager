require 'rails_helper'

RSpec.describe FamilyRole, type: :model do
  describe '.get_skills' do
    let!(:family) { Family.create(name: 'Sales') }
    let!(:role) { Role.create(name: 'VP') }
    let!(:family_role) { FamilyRole.create(family: family, role: role, values: [{:name=>"LXP", :value=>"I"}]) }

    it 'returns all skills for combination of family and role' do
      expect(FamilyRole.get_skills('Sales', 'VP')).to match_array [{:name=>"LXP", :value=>"I"}]
    end
  end
end
