require 'csv'

namespace :populate do
  task skills_data: :environment do
    CSV.foreach('db/skills_matrix.csv', headers: true).each_with_index do |row, i|
      ## Skip for proficiency row
      next if i == 1
      ## We create list of skill from first row
      if i == 0

        row.each_with_index do |value, j|
          Skill.find_or_create_by(name: value[0], key: j) unless [0,1].include? j
        end
      else
        ## Create familyRole from first two elements of row
        family = Family.find_or_create_by(name: row[0])
        role = Role.find_or_create_by(name: row[1], family: family)
        family_role = FamilyRole.find_or_create_by!(family: family, role: role)
        new_values = []

        ## Add skill values to the familyRole
        row.each_with_index do |value, j|
          unless [0,1].include? j
            new_values << {name: Skill.find_by(key: j)&.name, value: value[1]}
          end
        end
        family_role&.values = new_values
        family_role.save
      end
    end

  end
end
